# -*- coding: UTF-8 -*- 
import clipboard

def run_clipboard_tests():
    def test_empty_clipboard():
        assert(clipboard.gettext() == None)
        assert(clipboard.getblob() == None)
    
    def test_reset_clipboard():
        clipboard.reset()
        assert(clipboard.gettext() == None)
        assert(clipboard.getblob() == None)

    def test_copy_hindi_text():
        clipboard.reset()
        msg = u'विकिपीडिया:इण्टरनेट पर हिन्दी के साधन'
        clipboard.copytext(msg)
        text = clipboard.gettext()
        assert(msg == text)
        assert(clipboard.getblob() == None)

    def test_copy_english_text():
        clipboard.reset()
        clipboard.copytext("hello, world!")
        text = clipboard.gettext()
        assert(text == "hello, world!")
        assert(clipboard.getblob() == None)

    def test_copy_text_after_text():
        clipboard.reset()
        clipboard.copytext("utkarsh")
        text = clipboard.gettext()
        assert(text == "utkarsh")
        clipboard.copytext("hello")
        text = clipboard.gettext()
        assert(text == "hello")
        assert(clipboard.getblob() == None) 
       
   
    def test_copy_blob():
        clipboard.reset()
        f = open("images.jpeg", "rb")
        k = f.read()
        clipboard.copyblob(k)
        blob = clipboard.getblob()
        assert(blob == k)
        assert(clipboard.gettext() == None)


    def test_copy_blob_after_blob():
        clipboard.reset()
        f=open("images.jpeg","rb")
        k= f.read()
        clipboard.copyblob(k)
        blob = clipboard.getblob()
        assert(blob == k )
        assert(clipboard.gettext() == None)
        f=open("image1.jpeg","rb")
        p= f.read()
        clipboard.copyblob(p)
        blob =clipboard.getblob()
        assert(blob == p )
        assert(clipboard.gettext() == None)
        

    def test_copy_blob_after_text():
        clipboard.reset()
        clipboard.copytext("hello, world!")
        text = clipboard.gettext()
        f = open("image1.jpeg","rb")
        p = f.read()
        clipboard.copyblob(p)
        blob = clipboard.getblob()
        assert(text == "hello, world!")
        assert(blob == p)

    def test_copy_text_after_blob():
        clipboard.reset()
        f=open("image1.jpeg","rb")
        p= f.read()
        clipboard.copyblob(p)
        blob =clipboard.getblob()
        clipboard.copytext("hello, world!")
        text = clipboard.gettext()
        assert(blob == p and text == "hello, world!")
        
   
    def test_copy_text_with_size():
        clipboard.reset()
        f=open("test.py","r")  
        p=f.read()
        clipboard.copytext(p)
        text = clipboard.gettext()
        assert(text == p)
        assert(clipboard.getblob() == None)
   

    test_empty_clipboard()
    test_reset_clipboard()
    test_copy_english_text()
    test_copy_hindi_text()
    test_copy_text_after_text()
    test_copy_blob()
    test_copy_blob_after_blob()
    test_copy_blob_after_text()
    test_copy_text_with_size()

run_clipboard_tests()


def run_clipboard_observer_tests():
    def test_one_observer():
        def anobserver(reason):
            print "observer notified. reason: ", reason

        clipboard.reset()
        clipboard.addobserver(anobserver)
        #clipboard.copytext("hello, world!")
    
    test_one_observer()

run_clipboard_observer_tests()
