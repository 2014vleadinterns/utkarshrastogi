import sys
import re
import random
import pickle
import json
__all__ = ["add_note","find_note_title","find_body","dele_note","getlist","reset"]
__lists__ = []
__index__ = 0


def add_note(title,body):
     global __lists__
     global __index__
     if title == None:
        title = "untitled" + str(__index__)
        __index__+=1
     c=(title,body)
     __lists__.append(c)
     #print __lists__


def find_note_title(title):
     global __lists__
     temp=[]
     if title == None:
          temp=__lists__[:]
          #print __lists__,temp
     else:        
         for i in __lists__ :
            x=re.search(title,i[0])
            if x:
                temp.append(i)
                #print x.group()  
     return temp       
def find_body(body):
    global __lists__
    temp=[]
    for i in __lists__ :
        x=re.search(body,i[1])
        if x:
           temp.append(i)
           #print x.group()   
    return temp        
def dele_note(title):
     global __lists__
     i=0
     while i < len(__lists__) :
         x=re.search(title,__lists__[i][0])
         if x:
              
             __lists__.remove(__lists__[i])
             #print __lists__
	     i-=1	 
	 i+=1
     return __lists__
def save_note_pickle(p):
    f=open('list.txt','w')
    f.seek(0)
    # Creating a new Pickler.
    pickler = pickle.Pickler(f)
    # Dump python object to file.
    pickler.dump(p) 
    
def load_note_pickle():
    f=open('list.txt','r+')
    f.seek(0)
    pickler = pickle.Unpickler(f)
    p=pickler.load() 
    return p

def save_note_json(p):
    f=open('file.json','w+')
    json.dump(p,f)
def load_note_json():
    f=open('file.json','r+')
    f.seek(0)
    p=json.load(f)
    return p  

def getlist():
    global __lists__
    return __lists__   
def reset():
    global __lists__
    global __index__   
    __lists__ = []
    __index__ =None
