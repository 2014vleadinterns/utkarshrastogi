# -*- coding: UTF-8 -*- 
import note

def run_note_tests():
    def test_empty_note():
       assert(note.getlist() == [])
    def test_add_to_empty():
       note.reset()
       note.add_note("123456789","987654321")
       assert(note.getlist()==[("123456789","987654321")])
    def test_add_note():
       note.reset()
       note.add_note("delhi","Qutab Minar")
       note.add_note("mumbai","gateway of india")
       note.add_note("name","hello world")
       note.add_note("whats your name","utk")
       note.add_note("hello","utkarsh rastogi")
       note.add_note("utk","vvvv")
       assert(note.getlist() == [("delhi","Qutab Minar"),("mumbai","gateway of india"),("name","hello world"),("whats your name","utk"),("hello","utkarsh rastogi"), ("utk","vvvv")]) 
       
    def test_find_note_title():
       title = "utk"
       x = note.find_note_title(title)
       assert(x==[('utk', 'vvvv')])


    def test_find_body():
       body= "world"
       x = note.find_body(body)
       assert(x==[('name', 'hello world')])


    def test_find_note_by_none():
       title =None
       x=note.find_note_title(title)
       #print  x
       assert(x==[('delhi', 'Qutab Minar'), ('mumbai', 'gateway of india'), ('name', 'hello world'), ('whats your name', 'utk'), ('hello', 'utkarsh rastogi'), ('utk', 'vvvv')])

    
    def test_delete_note():
       title = "utkarsh"
       x = note.dele_note(title)
       #print x
       assert(note.getlist()==[('delhi', 'Qutab Minar'), ('mumbai', 'gateway of india'), ('name', 'hello world'), ('whats your name', 'utk'), ('hello', 'utkarsh rastogi'), ('utk', 'vvvv')])

    def test_delete_empty_list():
       note.reset()
       x = note.dele_note("utk")
       #print x
       assert(x==[])
    def test_save_pickle():
       note.reset()
       note.add_note("delhi","Qutab Minar")
       note.add_note("mumbai","gateway of india")
       assert(note.getlist() == [("delhi","Qutab Minar"),("mumbai","gateway of india")])
       note.save_note_pickle(note.getlist())
       r=note.load_note_pickle()
       assert(r==[("delhi","Qutab Minar"),("mumbai","gateway of india")])
    def test_save_json():
       note.reset()
       note.add_note("delhi","Qutab Minar")
       note.add_note("mumbai","gateway of india")
       assert(note.getlist() == [("delhi","Qutab Minar"),("mumbai","gateway of india")])
       note.save_note_json(note.getlist())
       r=note.load_note_json()
       assert(r==[["delhi","Qutab Minar"],["mumbai","gateway of india"]])
  
    test_empty_note()
    test_add_to_empty()
    test_add_note()
    test_find_note_title()
    test_find_body()
    test_find_note_by_none()
    test_delete_note()
    test_delete_empty_list()
    test_save_pickle()
    test_save_json()
   
run_note_tests()
